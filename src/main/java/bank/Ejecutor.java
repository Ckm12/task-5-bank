package bank;

import java.util.List;
import java.util.ArrayList;
import rules.*;

public class Ejecutor {
    List<ReglaDeNegocio> reglas = new ArrayList<>();
    public Ejecutor(List<ReglaDeNegocio> reglas) {
    this.reglas=reglas;
    }

    public  List<String> moverDinero (Transaccion transaccion) {

        List<String> reglaVioladas = new ArrayList<>();



        reglas.forEach(regla -> {
            String reglaViolada = regla.validate(transaccion);
            if (!reglaViolada.isEmpty()) {
                reglaVioladas.add(reglaViolada);
            }

        });
        return reglaVioladas;

    }
}



/**
        //Transferencia con valor igual a cero
            ReglaCero reglaCero = new ReglaCero();
            String ceroViolada = reglaCero.transaccionCero(transaccion);
            if(!ceroViolada.isEmpty()){
                reglaVioladas.add(ceroViolada);
            }

            //Transaccion valor transferencia máxima
            ReglaTransaccionMaxima reglaTransaccionMaxima = new ReglaTransaccionMaxima();
            String transaccionMaximaViolada = reglaTransaccionMaxima.transaccionMaxima(transaccion);
            if (!transaccionMaximaViolada.isEmpty()){
                reglaVioladas.add(transaccionMaximaViolada);
            }

            //Regla chip
            ReglaChip reglaChip = new ReglaChip();
            String transaccionChipViolada = reglaChip.transaccionChip(transaccion);
            if(!transaccionChipViolada.isEmpty()){
                reglaVioladas.add(transaccionChipViolada);
            }

            // cuenta activa
            ReglaActiva reglaActiva = new ReglaActiva();
            String activaViolada = reglaActiva.cuentaActiva(transaccion);
            if (!activaViolada.isEmpty()){
                reglaVioladas.add(activaViolada);
            }

            //Monto soportado en cuenta
            MontoSoportado montoSoportado = new MontoSoportado();
            String montoSoportadoViolado = montoSoportado.montoSoportado(transaccion);
            if (!montoSoportadoViolado.isEmpty()){
                reglaVioladas.add(montoSoportadoViolado);
            }

            return reglaVioladas;

        }
    }

**/


