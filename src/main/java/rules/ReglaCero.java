package rules;

import bank.Transaccion;

public class ReglaCero implements ReglaDeNegocio {
    public String validate(Transaccion transaccion){
        if (transaccion.valor == 0){

            return "Regla violada: El monto igual a 0";
        }
        return "";
    }
}
