package rules;

import bank.Transaccion;

public class MontoSoportado implements ReglaDeNegocio {

    public String validate(Transaccion transaccion){
        if (transaccion.valor > transaccion.cuenta.monto){
            return "Regla violada: saldo insuficiente";
        }
        return "";
    }
}
