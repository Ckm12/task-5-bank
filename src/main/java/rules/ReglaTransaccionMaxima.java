package rules;

import bank.Transaccion;

public class ReglaTransaccionMaxima implements ReglaDeNegocio {

    public String validate(Transaccion transaccion){
        if (transaccion.valor > 100000){

            return "Regla violada, monto superior a 100000";
        }
        return "";

    }
}
