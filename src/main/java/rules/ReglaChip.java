package rules;

import bank.Transaccion;

public class ReglaChip implements ReglaDeNegocio {

    public String validate(Transaccion transaccion){
        if (transaccion.cuenta.tarjetaChip= false){
            return "Regla violada: La tarjeta debe ser de chip para pode continuar la transacción";
        }

        return "";
    }
}
