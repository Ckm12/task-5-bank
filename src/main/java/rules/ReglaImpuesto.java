package rules;

import bank.Transaccion;

public class ReglaImpuesto implements  ReglaDeNegocio{
    public String validate(Transaccion transaccion){
        if (transaccion.valor > transaccion.cuenta.monto){

            return "Regla violada ; No tienes Dinero para pagar el impuesto por transaccion";
        }
        return "";

    }
}
