package rules;

import bank.Transaccion;

public class ReglaActiva implements ReglaDeNegocio {
    public String validate(Transaccion transaccion){
        if (!transaccion.cuenta.activa){

            return "Regla violada: Cuenta inactiva";
        }
        return "";
    }
}
