package rules;

import bank.Transaccion;

public class ReglaTransaccionMinima  implements ReglaDeNegocio{

    public String validate(Transaccion transaccion){
        if (!(transaccion.valor > 10000)){
            return "Regla violada: El valor a transar debe ser superior a 10000";
        }

        return "";
    }
}

